/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package atrea;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;
import com.jme3.network.serializing.Serializer;
import java.util.HashMap;

/**
 *
 * @author Petr
 */
public class Networker {
    
    public static final int TCPPORT = 6342;
    public static final int UDPPORT = 6345;
    public static final String SERVER_NAME = "Atrea Server";
    public static final int SERVER_VERSION = 1;
    
    
    public static void InitiliaseSerializables(){
        Serializer.registerClass(SystemMessage.class);
        Serializer.registerClass(BasicMessage.class);
        Serializer.registerClass(LocAndDirMessage.class);
        Serializer.registerClass(WorldUpdateMessage.class);
        Serializer.registerClass(Player.class);
    }
    
    @Serializable
    public static class BasicMessage extends AbstractMessage {

        private String msg;

        public BasicMessage() {
            this.msg = "";
        }

        public BasicMessage(String message) {
            this.msg = message;
        }

        public String getMessage() {
            return this.msg;
        }
    }
    
    @Serializable
    public static class LocAndDirMessage extends AbstractMessage {
        
        private Vector3f position;

        private Quaternion direction;
        
        public LocAndDirMessage(){}
        
        public LocAndDirMessage(Vector3f pos, Quaternion dir){
            this.position = pos;
            this.direction = dir;
        }
        
        public Vector3f getPosition() {
            return position;
        }

        public Quaternion getDirection() {
            return direction;
        }
    }
    
    public enum SystemMessageType {
        JOIN_GAME, LEAVE_GAME
    }
    
    @Serializable
    public static class SystemMessage extends AbstractMessage {
        private Networker.SystemMessageType type;
        private int playerId;

        public SystemMessage() {
        }
        
        public SystemMessage(SystemMessageType t, int playerId) {
            this.type = t;
            this.playerId = playerId;
        }
        
        public SystemMessageType getType(){
            return this.type;
        }
        
        public int getPlayer(){
            return this.playerId;
        }
    }
    
    @Serializable
    public static class WorldUpdateMessage extends AbstractMessage {
        private HashMap<Integer,Player> players;
        
        public WorldUpdateMessage(){}
        
        public WorldUpdateMessage(HashMap<Integer,Player> players){
            this.players = players;
        }
        
        public HashMap<Integer,Player> getWorldUpdate(){
            return this.players;
        }
            
    }
}
