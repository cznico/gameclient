package atrea;

import com.jme3.app.SimpleApplication;
import com.jme3.asset.plugins.ZipLocator;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.network.Client;
import com.jme3.network.Network;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.system.JmeContext;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import atrea.Networker.BasicMessage;
import atrea.Networker.LocAndDirMessage;
import atrea.Networker.SystemMessage;
import atrea.Networker.SystemMessageType;

/**
 * @author Petr Holec
 */
public final class ClientMain extends SimpleApplication implements ActionListener {
    
    private Client client;
    
    private Spatial sceneModel;
    private BulletAppState bulletAppState;
    private RigidBodyControl landscape;
    private CharacterControl player;
    private Vector3f walkDirection = new Vector3f();
    private boolean left = false, right = false, up = false, down = false;
    
    private static int clientId = -1;
    private static ClientMain clientApp;
    private static HashMap<Integer, PlayerClient> players = new HashMap<Integer, PlayerClient>();
    
    private Vector3f camDir = new Vector3f();
    private Vector3f camLeft = new Vector3f();
    
    public static void main(String[] args) {
        clientApp = new ClientMain();
        clientApp.setPauseOnLostFocus(false);
        clientApp.start(JmeContext.Type.Display);
        Networker.InitiliaseSerializables();
    }

    @Override
    public void simpleInitApp() {
        try {
            client = Network.connectToServer(Networker.SERVER_NAME, Networker.SERVER_VERSION, "localhost", Networker.TCPPORT, Networker.UDPPORT);
            client.addMessageListener(new ClientMessageListener(), BasicMessage.class, SystemMessage.class, Networker.LocAndDirMessage.class, Networker.WorldUpdateMessage.class);
            
            client.start();
            client.send( new SystemMessage(SystemMessageType.JOIN_GAME, -1) );
        } catch (IOException ex) {
            Logger.getLogger(ClientMain.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //Geometry g = new CreateGeoms(this).createBox();
        //rootNode.attachChild(g);
        
        
        /** Set up Physics */
        bulletAppState = new BulletAppState();
        stateManager.attach(bulletAppState);
        //bulletAppState.getPhysicsSpace().enableDebug(assetManager);

        // We re-use the flyby camera for rotation, while positioning is handled by physics
        viewPort.setBackgroundColor(new ColorRGBA(0.7f, 0.8f, 1f, 1f));
        flyCam.setMoveSpeed(100);
        setUpKeys();
        setUpLight();

        // We load the scene from the zip file and adjust its size.
        assetManager.registerLocator("assets/Scenes/town.zip", ZipLocator.class);
        sceneModel = assetManager.loadModel("main.scene");
        sceneModel.setLocalScale(2f);

        // We set up collision detection for the scene by creating a
        // compound collision shape and a static RigidBodyControl with mass zero.
        com.jme3.bullet.collision.shapes.CollisionShape sceneShape = CollisionShapeFactory.createMeshShape((Node) sceneModel);
        landscape = new RigidBodyControl(sceneShape, 0);
        sceneModel.addControl(landscape);

        // We set up collision detection for the player by creating
        // a capsule collision shape and a CharacterControl.
        // The CharacterControl offers extra settings for
        // size, stepheight, jumping, falling, and gravity.
        // We also put the player in its starting position.
        CapsuleCollisionShape capsuleShape = new CapsuleCollisionShape(1.5f, 6f, 1);
        player = new CharacterControl(capsuleShape, 0.05f);
        player.setJumpSpeed(20);
        player.setFallSpeed(30);
        player.setGravity(30);
        player.setPhysicsLocation(new Vector3f(0, 10, 0));

        // We attach the scene and the player to the rootnode and the physics space,
        // to make them appear in the game world.
        rootNode.attachChild(sceneModel);
        bulletAppState.getPhysicsSpace().add(landscape);
        bulletAppState.getPhysicsSpace().add(player);
    }   
    
    private void setUpLight() {
        // We add light so we see the scene
        AmbientLight al = new AmbientLight();
        al.setColor(ColorRGBA.White.mult(1.3f));
        rootNode.addLight(al);

        DirectionalLight dl = new DirectionalLight();
        dl.setColor(ColorRGBA.White);
        dl.setDirection(new Vector3f(2.8f, -2.8f, -2.8f).normalizeLocal());
        rootNode.addLight(dl);
      }
    
    @Override
    public void simpleUpdate(float tpf) {
        camDir.set(cam.getDirection()).multLocal(0.6f);
        camLeft.set(cam.getLeft()).multLocal(0.4f);
        walkDirection.set(0, 0, 0);
        if (left) {
            walkDirection.addLocal(camLeft);
        }
        if (right) {
            walkDirection.addLocal(camLeft.negate());
        }
        if (up) {
            walkDirection.addLocal(camDir);
        }
        if (down) {
            walkDirection.addLocal(camDir.negate());
        }
        player.setWalkDirection(walkDirection);
        cam.setLocation(player.getPhysicsLocation());
        
        clientApp.sendPositionMessage();
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }
    
    public static void SetClientId(int id){
        clientId = id;
    }
    
    public static void RemovePlayer(int hostId){
        players.remove(hostId);
    }
    
    public static ClientMain GetInstance(){
        return clientApp;
    }
    
     
    public static void UpdatePlayers(HashMap<Integer,Player> pl){
        Iterator<Integer> it = pl.keySet().iterator();

        while (it.hasNext()){
            Integer index = it.next();
            if (players.containsKey(index)){
                players.get(index).setDirection(pl.get(index).getDirection());
                players.get(index).setPosition(pl.get(index).getPosition());
            } else {
                Spatial g = new CreateGeoms(clientApp).createBox(); 

                System.out.println("New playerd joined with id:" + index);
                clientApp.rootNode.attachChild(g);
                players.put(index, new PlayerClient(g));
            }
        }
    }
    
    public void sendPositionMessage(){
        if (clientId == -1) return;
        /*client.send(new LocAndDirMessage(
                players.get(clientId).getSpatial().getLocalTranslation(),
                players.get(clientId).getSpatial().getWorldRotation()
        ));*/
        // TODO dodelat i otoceni
        client.send(new LocAndDirMessage(
                player.getPhysicsLocation(),
                Quaternion.IDENTITY
        ));
    }
    
    @Override
    public void destroy(){
        try {
            client.send( new SystemMessage(SystemMessageType.LEAVE_GAME, clientId) ); // patrne nikdy nedorazi, protoze se hned potom uzavre spojeni
            client.close();
        } catch (Exception ex){
            Logger.getLogger(ClientMain.class.getName()).log(Level.SEVERE, null, ex);
        }
        super.destroy();
    }
    
    /** We over-write some navigational key mappings here, so we can
   * add physics-controlled walking and jumping: */
  private void setUpKeys() {
    inputManager.addMapping("Left", new KeyTrigger(KeyInput.KEY_A));
    inputManager.addMapping("Right", new KeyTrigger(KeyInput.KEY_D));
    inputManager.addMapping("Up", new KeyTrigger(KeyInput.KEY_W));
    inputManager.addMapping("Down", new KeyTrigger(KeyInput.KEY_S));
    inputManager.addMapping("Jump", new KeyTrigger(KeyInput.KEY_SPACE));
    inputManager.addListener(this, "Left");
    inputManager.addListener(this, "Right");
    inputManager.addListener(this, "Up");
    inputManager.addListener(this, "Down");
    inputManager.addListener(this, "Jump");
  }
 
  /** These are our custom actions triggered by key presses.
   * We do not walk yet, we just keep track of the direction the user pressed. */
  public void onAction(String binding, boolean isPressed, float tpf) {
    if (binding.equals("Left")) {
      left = isPressed;
    } else if (binding.equals("Right")) {
      right= isPressed;
    } else if (binding.equals("Up")) {
      up = isPressed;
    } else if (binding.equals("Down")) {
      down = isPressed;
    } else if (binding.equals("Jump")) {
      if (isPressed) { player.jump(); }
    }
  }
}
