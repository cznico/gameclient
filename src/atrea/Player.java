/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package atrea;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.network.serializing.Serializable;

/**
 *
 * @author Petr
 */
@Serializable
public class Player {
    private Vector3f position;
    private Quaternion direction;

    public Player() {
        position = Vector3f.ZERO;
        direction = Quaternion.IDENTITY;
    }

    public Player(Vector3f position, Quaternion direction) {
        this.position = position;
        this.direction = direction;
    }

    public Vector3f getPosition() {
        return position;
    }

    public Quaternion getDirection() {
        return direction;
    }

    public void setPosition(Vector3f position) {
        this.position = position;
    }

    public void setDirection(Quaternion direction) {
        this.direction = direction;
    }
    
}
