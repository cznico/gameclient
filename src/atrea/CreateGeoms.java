/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package atrea;

import com.jme3.app.SimpleApplication;
import com.jme3.material.Material;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;

/**
 *
 * @author Petr
 */
public class CreateGeoms {

    private Material m;
    private Box b;

    public CreateGeoms(SimpleApplication app) {
        m = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
        b = new Box(Vector3f.ZERO, new Vector3f(1, 2, 1));
    }
    
    public Spatial createBox(){
        Geometry box = new Geometry ("box", b);
        box.setMaterial(m);
        return box;
    }
}
