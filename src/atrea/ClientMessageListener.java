/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package atrea;

import com.jme3.network.Client;
import com.jme3.network.Message;
import com.jme3.network.MessageListener;
import java.util.concurrent.Callable;
import atrea.Networker.BasicMessage;
import static atrea.Networker.SystemMessageType.JOIN_GAME;
import static atrea.Networker.SystemMessageType.LEAVE_GAME;
import atrea.Networker.WorldUpdateMessage;

/**
 *
 * @author Petr
 */
public class ClientMessageListener implements MessageListener<Client> {

    public void messageReceived(Client source, Message m) {
        if (m instanceof Networker.SystemMessage) {

            Networker.SystemMessage msg = (Networker.SystemMessage) m;
            switch ( msg.getType() ){
                case JOIN_GAME:
                    ClientMain.SetClientId(source.getId());
                    break;
                case LEAVE_GAME:
                    System.out.println("Client #" + source.getId() + " disconnected");
                    ClientMain.RemovePlayer(source.getId());
                    break;
            }
        } else if (m instanceof BasicMessage) {
            BasicMessage msg = (BasicMessage) m;
            System.out.println("Client #" + source.getId() + " received: '" + msg.getMessage() + "'");
        } else if (m instanceof WorldUpdateMessage){
            final WorldUpdateMessage msg = (WorldUpdateMessage) m;
            ClientMain.GetInstance().enqueue(new Callable() {

                public Object call() throws Exception {
                    ClientMain.UpdatePlayers(msg.getWorldUpdate());
                    return null;
                }
            });
        }
    }
}
