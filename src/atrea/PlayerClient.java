/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package atrea;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;

/**
 *
 * @author Petr
 */
public class PlayerClient {
    private Spatial sobject;

    public PlayerClient(Spatial s) {
        sobject = s;
    }

    public Vector3f getPosition() {
        return sobject.getLocalTranslation();
    }

    public Quaternion getDirection() {
        return sobject.getLocalRotation();
    }

    public void setPosition(Vector3f position) {
        sobject.setLocalTranslation(position);
    }

    public void setDirection(Quaternion direction) {
        sobject.setLocalRotation(direction);
    }
    
    public Spatial getSpatial(){
        return sobject;
    }
    
}
